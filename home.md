---
title: Decentrala
description: Decentrala je zajednica okupljena oko decentralizacije tehnologija i sirenja znanja.
published: true
date: 2023-02-15T18:53:04.173Z
tags: 
editor: markdown
dateCreated: 2023-02-15T10:37:47.784Z
---


# Znanje

Svako moze drzati predavanje na bilo koju temu, samo se najavite na forumu.

Decentrala veruje da svako treba da ima pristup znanju, zbog cega su predavanja uvek besplatna i bez promocija.

Ako zelite nesto da podelite sa drugima sto ste naucili, prijavite se da drzite jedno ili vise predavanja!

# Akcija

Organizovanje dogadjaja programiranja ili instalacije servisa koje pomazu u decentralizaciji Interneta.

Organizujemo hackathone kao posebne dogadjaje na kojima se okupljamo da radimo na zajednickom cilju.

# Druzenje

Drustvene dogadjaje u cilju socijalizacije.

Ako zelite da se druzite sa ljudima zainteresovanim za ravnopravnost koriscenja tehnologija, slobodnog softvera, privatnost i bezbednost, posetite neku od okupljanja Decentrale. 