---
title: Python kurs - kurikulum
description: 
published: true
date: 2023-03-25T09:02:23.718Z
tags: python
editor: markdown
dateCreated: 2023-02-26T15:54:10.597Z
---


[1. čas](https://radionice.dmz.rs/python/cas1.html)

* Šta je programiranje, šta je Python?
* Skidamo Python i IDE i instaliramo ih
* Prvi program i njegovo pokretanje
* Komentarisanje koda
* Promenljive
* Aritmetičke operacije
* Debugging
* ulaz sa konzole
* Tipovi podataka
* Boolean, broj i string
* Domaći: napisati program koji izračuna BMI

[2. čas](https://radionice.dmz.rs/python/cas2.html)

* Ponavljanje prvog časa
* if, elif, else
* ugnježdene if naredbe
* Domaći: program za konvertovanje temperature

[3. čas](https://radionice.dmz.rs/python/cas3.html)

* Ponavljanje drugog časa
* Operatori poređenja
* while petlja
* break, continue
* Domaći: napisati program kojim korisnik pogađa broj koji je zamislio računar

4. čas

* Liste (kreiranje liste, dodavanje elemenata u listu)
* for petlja
* for petlja sa range-om
* ugnježdene petlje
* Funkcije (sistemske funckije, korisničke funkcije)
* operacije sa stringovima
* formatiranje stringova
* tuple tip
* Domaći: napraviti funkciju koja proveri da li je string palindrom

5. čas

* dictionary tip
* iteracija kroz dictionary
* rekurzivne funkcije
* pisanje i čitanje fajlova
* Domaći: učitati CSV i ispisati redove sortirano

6. čas

* Osnove OOP-a

7. čas

* testiranje koda
* PyTest

8. čas

* sve što nam je (za)ostalo
* oblasti programiranja (frontend, backand, AI, mobile, baze podataka…)
* random diskusije o bilo čemu
