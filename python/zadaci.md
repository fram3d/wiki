---
title: Zadaci za kurs
description: Predlozi zadataka za kurs u KC Gradu
published: true
date: 2023-02-27T19:02:01.402Z
tags: python, kc grad, radionica, kurs, zadaci
editor: markdown
dateCreated: 2023-02-27T18:47:28.648Z
---

# Predlozi zadatka
# RADNA VERZIJA

## MadLib

[MadLib](https://en.wikipedia.org/wiki/Mad_Libs) je tekstualna igra u kojoj igrac/i na kraju dobiju pricu. Osnovna verziju je lako implementirati u pajtonu.

```python
def main():
	imenica = input("Unesi imenicu")
  glagol = input("Unesi glagol")
  zamenica = input("Unesi zamenicu")
  
  prica = sastavi_pricu(imenica=imenica, glagol=glagol, zamenica=zamenica)
  print(prica)
  
def sastavi_pricu(imenica, glagol, zamenica):
	prica = f"{imenica} je {glagol} za {zamenica}"
  return prica
 
 
if __name__ == "__main__":
	main()
```

Ovde sada ima mesta za doradu, da se dodaju provere da li je uneta bas rec, da li je zamenica iz liste zamenica, glagol iz liste glagola itd. I naravno moze jos input-a da se dodaje i da se pravi zanimljivija prica.

## Pogodi broj

Komp zamisli broj igrac pogadja, ovde se prikazuje `while` petlja i provera `if`.

```python
import random

def main():
	zamisljeni_broj = random.random(100)
  pokusani_broj = input("Pogodi broj:")
  while int(pokusani_broj) != zamisljeni_broj:
  	if int(pokusani_broj) > zamisljeni_broj:
    	print("Zamisljeni broj je manji, probaj opet")
      pokusani_broj = input("...")
  	if int(pokusani_broj) < zamisljeni_broj:
    	print("Zamisljeni broj je veci, probaj opet")
      pokusani_broj = input("...")
  print("Bravo pogodili ste broj!")
```


## Konverter

Da nesto konvertuje, temperatura, masa, valute, vreme.

## Bioinformatika

Neki jednostavniji [odavde](https://rosalind.info/problems/list-view/), kroz ovo se nauci neki rad sa stringovima.

## XOR sifrovanje

Sifrovanje sa xorom, one-time-pad, kroz ovo se pokazu bitovski operatori i gde se stvarno koriste.

## Vizualizacija podataka

Za ovo moze  da se ucita csv i da se koristi matplotlib i pandas takodje. 

## Processing.py

Sa ovim mozemo da crtamo i recimo pokazemo upotrebu sinusa, kosinusa kroz crtanje. Mogu se praviti lepe slike, na [primer](https://en.wikipedia.org/wiki/Rose_(mathematics)), isprogramira se ova formula. 

## Scraping

Skidanje nekog sajta vesti i vadjenje svih linkova ka vestima koja sadrze neku kljucnu rec.

## Web - FLask

Napravi se webapp za neku od prethodnih skripti. Na primer za scraping, ili xor siforvanje.

## Rad sa bazom - todo lista

Pokaze se sqlite i flask, aplikacija cuva jednostavnu todo sifru.