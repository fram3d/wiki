---
title: Python - liste
description: O listama u python-u
published: true
date: 2023-02-04T23:14:55.267Z
tags: 
editor: markdown
dateCreated: 2023-02-04T23:06:54.851Z
---

# Python - liste

Liste u python (just trying some things out)

Sluze nam da 'drzimo' vise vrednosti u jednoj promenljivoj. Te vrednosti mogu biti bilo kog tipa, i moze ih biti neograniceno mnogo (ograniceno samo memorijom vaseg racunara).

```python
list = ["Beograd", "Novi Sad", "Nis", "Subotica"]
print(list)
# > ['Beograd', 'Novi Sad', 'Nis', 'Subotica']
```

Mozemo pristupati elementima liste preko `lista[i]` sintakse, gde je `i` index zeljene vrednosti u datoj listi. *Vazno*: indeksi pocinju od 0 a ne od 1!

```python
print(list[0])
# > Beograd
```