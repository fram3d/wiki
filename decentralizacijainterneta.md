---
title: Decentralizacija Interneta
description: Vaznost i strategija decentralizacije Interneta
published: true
date: 2023-02-15T10:43:03.962Z
tags: 
editor: markdown
dateCreated: 2023-02-15T10:42:25.528Z
---

# Decentralizacija Interneta

## Decentralizacija je bitna
Danas tehnologija mnogo utice na nase zivote.
Glavni izvor informacija i nacin komunikacije se odvija preko Interneta.

Kada odluke o sadrzaju i strukturi Interenta odlucuje mali broj ljudi, njihovi interesi mogu biti drugaciji od interesa celog drustva.

## Trenutno stanje Interneta
Samo par kompanija trenutno upravlja skoro celim Internetom.
Mali broj ljudi koji njim upravlja cesto ima interes da promovise strah i mrznju radi privlacenja paznje njihovom sadrzaju i prikupljanju profita od reklama koje im se tom prilikom prikazuju.
Promovisanje straha i mrznje utice na povisen broj psihickih problema kod korisnika i povisenom nasilju u drustvu.
Svi su prisiljeni da koriste ove platforme, jer se trazenje posla se odvija na njima. Kao i svakodnevna drustvena interakcija, koja je neophodna za psihicko zdravlje ljudi. Vazne informacije za svakodnevni zivot se takodje odvija preko ovih platformi, kao i ucestvovanje u vaznim diskusijama i odlukama.

Privatnost i bezbednost korisnika je dovoljno velika, da je primetna vecini korisnika, sto utice na rast reklamiranja i upotrebe neadekvatnih resenja, kao sto su VPN provajderi. Povezivanje uredjaja direktno na mrezu VPN provajdera uklanja bezbednost omogucenu od rutera na koji je korisnik povezan i dodatno centralizuje protok informacija u mali broj VPN provajdera. Ovakva lazna resenja cine problem jos gorim za pojedince i drustvo kao celinu.

## Opasnosti
Sa velikim razvojem algoritama za predvidjanje interakcije korisnika na ovim platformama, raste preciznost u promovisanju informacija od interesa kompanijama koje drze ove platforme. Uspesnost u zatrpavanju informacija cije sirenje nije u interesu ovih kompanija takodje raste sa razvojem ovih algoritama.
Ovakvi algoritmi, koji koriste neuronske mreze, nije moguce direktno nadgledati od strane ljudskih bica, nego se razvijaju automatski, trenirani nad podacima korisnika ovih platformi.
Ovakva centralizcija moci pristupa informacija negativno utice na demokratiju i drustvenu ravnopravnost.

Privatnost i samim tim i bezbednost korisnika je ugrozena zbog velike kolicine licnih informacija prikupljene o njihovim zivotima i pretrazivanjima na Internetu. Pristup velikoj kolicini informacija od malog broja ljudi, pravi pogodne mete od tih pojedinaca za kradju podataka, kao i zloupotrebu privatnih informacija od samih tih pojedinaca.

## Resenje
Zbog vece koristi za korisnika od mreza koje ih povezuju sa vecim brojem ljudi tesko je napraviti konkurenciju vec postojecim mrezama koje sadrze vecinu svetske populacije. Postepena decentralizacija je lakse izvodljiva sa prvobitnom dostupnoscu na decentralizovanim platformama pred potpunu tranziciju na njih.
Cena instaliranja jedne dodane aplikacije za bilo koju decentralizaovanu platformu, sa otvorenim protokolom, je znatno manja od dugorocnog benefita postignutog decentralizacije Interneta. Dovoljno je da vecina instalira jednu aplikaciju, slobodnog i otvorenog koda, koja daje pristup decentralizovanoj mrezi da bi tranzicija na decentralizovane platforme bila izvodljiva. U ovakvoj situaciji, lako je ubediti vecinu da koristi iskljucivo aplikacije decentralizovanih platformi, zbog ogrome i primetne cene privatnosti i slobodi informisanja na centralizovanim platformama.

Neke od trenutnih decentralizovanih platformi za informisanje su XMPP (Jabber), Matrix i Email.
Od kojih email ima daleko najvecu popularnost, ali veliku tehnicku ogranicenost.