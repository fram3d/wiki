---
title: Conversations
description: How to install conversations and join groups
published: true
date: 2023-07-02T22:10:17.873Z
tags: 
editor: markdown
dateCreated: 2023-07-02T22:10:17.873Z
---

# Conversations

## Intro

We will go over installation of Conversations app that will be simmilar to [this](https://vid.puffyan.us/watch?v=FZ2Goa8VQ5w) video.

Conversations is a chat application that runs on Android. There are other apps that you could run on your computer or iPhone that will let you talk to people using Conversations.

Unlike most mainstream chat apps, it is not run by a company. It's code is fully avaliable online for anyone to read, modify or share. That means that it is developed explicitly with user's interests in mind, since anyone can just change the code and share it, if some unpopular feature is added to the code. Nothing in Conversations will harm or inconvinience users in any way, so common practices such as collecting personal information to sell to advertisers and adding addictive features to try and keep users attention do not exist in Conversations.

There are no central servers run by the developers, anyone can be a developer and anyone can run a server for Conversations.

## Installation

There are multiple ways of installing Conversations, the best way is to install it using the [F-Droid](https://f-droid.org) app.

F-Droid is a open and privacy respecting alternative to Google Play. Since Google Play doesn't allow competition F-droid can not be found in Google Play store and has to be downloaded from F-Droid's website at [f-droid.org](https://f-droid.org/F-Droid.apk).

After downloading F-Droid you can install it on your Andorid phone by opening on that downloaded file.

Open your newly installed f-droid app. You will need to wait for few minutes while it is updating the list of available apps. Click the search (spyglass) button at the bottom right of the screen and type Conversations in the search bar at the top. Sometimes it is not on the top of the search results, but you can scroll down until you find it (it has a green icon with a white conversation bubble in the middle and three dots). After clicking on it it will open a new screen with more information about the app. Click "Install" and after it is downloaded you will get a pop-up that says that your phone is not allowed to install apps from an unknown source. Click "Settings" and move the slider that says "Allow from this source". This will allow you to install apps from F-Droid.

After installation you will go back to the F-Droid screen where you can open Conversations or just open it like any other app on your phone.

## Creating an account

Unlike most apps, Conversations lets you choose which server you will want to create an account on. You can run your own server, connect to a server of your local hackerspace or just pick a random server suggested on [this](https://compliance.conversations.im) website. This is same as e-mail, where you can pick your e-mail provider or run your own server.

Often you can create an account on the website of the server you choose, but sometimes you can do it by clicking "Create a new account" when you open Conversations. If you choose to click the "Create a new account" button, don't use the server of **conersations.im** that will be suggested to you. It is safer to pick a random server than for everyone to use the same one, we don't want our communication to be fully dependent on small group of people, like chat applications run by companies.

After you create an account you will have your address (just like e-mail address) and your password. Simply add it in Conversations app by clicking "I already have an account" in the main menu and follow the instructions.

Now you are ready to use the app.

## Using Conversations

To join a group chat you can click on the plus "+" sign in the bottom right corner of the main menu.

Some group chats are public, which means anyone can join, others are private and you need to be invited to join.

To join Decentrala public chat you can click on "Join public channel" and type "decentrala@conference.dmz.rs"