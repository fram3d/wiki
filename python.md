---
title: Python radionice u KC Gradu
description: 
published: true
date: 2023-03-25T09:03:24.488Z
tags: python
editor: markdown
dateCreated: 2023-02-15T19:08:31.803Z
---

# Uvod

Decentrala zajednica, u sklopu svojih akcija širenja znanja, pravi prvu besplatnu radionicu programiranja na kojoj su svi dobrodošli.

Fokus će biti na upoznavanje sa programiranjem i njegovu demistifikaciju, šta to zapravo znači kontrolisati mašine, kako to izgleda u praksi, prvi dodir sa programskim jezicima, šta to znači razmišljati na programerski način... U pitanju su **radionice**, a ne predavanja, što znači da se od svakog od vas traži **aktivno** učestvovanje, rad na času i kući. Za svo vreme trajanja radionica, uvek će tu biti i predavači i ljudi iz Decentrale koji će se truditi da Vam pomažu, odglavljuju i uvek ćete moći da nas sve pitate, što na času, što online.

Kao jezik na kome ćemo se upoznavati sa programiranjem smo odabrali Python, ali ovo nije kurs Python-a, mada ćete se kroz radionice upoznati sa njim vrlo detaljno.

Cilj radionica je da možete da budete funkcionalni u Python programskom jeziku i da pišete osnovni programski kôd. Takođe treba da budete osposobljeni da čitate tuđe programe i da ih menjate.

## Prijave

Da biste se prijavili za učestvovanje na radionici, potrebno je da se najavite (jednom samo, ne za svaki termin) na [ovom linku](https://cryptpad.fr/form/#/2/form/view/JSszbKIhK8Xp-+sER3AchSnyA2sfHEPsKle4UAikV4Y/). Ovim ćemo da imamo uvid u zainteresovanost ljudi.

## Slajdovi

Svi slajdovi sa predavanja se nalaze na adresi https://radionice.dmz.rs/python/.

## FAQ

Q: Ko može da prisustvuje radionicama?
A: Nema apsolutno nikakvih ograničenja po pitanju ko može da prisustvuje.

--- 

Q: Ko drži predavanja?
A: Predavanja će držati na smenu dva predavača iz industrije sa višegodišnjim iskustvom. Više o njima ćete saznati na samim predavanjima. Pored njih, biće tu još ljudi iz Decentrale koji će se truditi da pomažu za sva pitanja koje budete imali.

---

Q: Kakvo predznanje treba da imam da bih prisustvovala(o)?
A: Nije potrebno nikakvo predznanje, štaviše predavanja i jeste namenjeno ljudima bez ikakvog dodira sa programiranjem,

---

Q: Da li treba da imam laptop?
A: Poželjno je da imate svoj laptop. Ne morate ništa instalirati na njemu, sve ćemo to uraditi još na prvom času. Ukoliko nemate laptop, možete svakako doći, a mi ćemo se potruditi da nađemo neki priručni, da možete da pratite predavanja.

---

Q: Da li mogu da se zaposlim posle ovih radionica?
A: **Ne**. Cilj radionice je upoznavanje sa osnovama programiranja, ne osposobljavanje za rad. Možemo i mi da vas lažemo, ali to bolje rade privatne škole i IT akademije.

---

Q: Da li će postojati domaći, koliko moram da se "cimam" za ovu školu?
A: Da, biće domaći i biće obimni. Na vama je koliko želite da se cimate, a preporuka je da iskoristite ovu priliku najbolje što možete.

Q: Šta ako propustim neki čas?
A: Ništa, očekujemo vas na sledećem. Predavanja nisu nezavisna, gradivo se vezuje jedno na drugo i potrebno je da pogledate forum i da nađete materijale od prethodnog časa i da probate da ih prođete i uradite domaći.

## Format

Radionice će se odvijati u prostorijama KC Grada, ulica Braće Krsmanović 4 u Beogradu. Lokaciju možete videti [ovde](https://www.openstreetmap.org/node/4118716889).

Plan je da se održi 8+1 radionica, petkom od 18h-20h, i to u sledećim terminima:
* 10.03.
* 17.03.
* 24.03.
* 31.03.
* 07.04.
* 14.04.
* 21.04.
* 28.04.

07.04. i 14.04. ćemo biti u prostoru dole, dok smo ostalim terminima na spratu.

Ukoliko bude naknadnih neočekivanih izmena, objavićemo ih blagovremeno na forumu.

## Kontakt

Najbolji načini za kontakt su:
* [forum](https://forum.dmz.rs/t/python-radionica-u-kc-gradu/66) - ovde ćemo kačiti materijale i obaveštenja i ovde možete pitati sva pitanja
* [chat](https://dmz.rs/chat.html) - ovde možemo da vodimo neformalnu i protočnu diskusiju

Trudićemo se i da dođemo pre predavanja i da ostanemo posle istih, za konsultacije ukoliko ih imate.