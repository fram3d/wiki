---
title: Doprinos slobodnom softveru
description: Tehnicko znanje nije neophodno za doprinos slobodnom softveru
published: true
date: 2023-02-15T10:47:52.510Z
tags: 
editor: markdown
dateCreated: 2023-02-15T10:47:52.510Z
---


# Kako doprineti slobodnom softveru bez programiranja i administracije

Ne morate biti programer ili administrator da biste doprineli slobodnom softveru. Ovaj tekst navodi akcije koje doprinose dosta a da ne morate znati programiranje niti administraciju.
Upotreba slobodnog softvera

Ukoliko koristite slobodan softver (linux, gajim, cryptpad npr.) vec doprinosite. Doprins je u tome sto taj softver dobija novog koriniska, sto je samo po sebi super stvar! Takodje verovatno pricate o tom softveru medju drustvom i preporucujete ga (vise o ovome u sledecoj tezi), time dovodite druge ljude da ga isto koriste. Na primer cryptpad koristite za neko organizovanje, deljenje podataka i automatski drugi ljudi koriste to i sire drugim ljudima jer taj softver valja i radi posao dobro.
Prevodjenje

 Prevodjenje softvera na lokalne jezike (lokalizacija) je jako bitna jer ne znaju svi ljudi engleski. To omogucava ljudima da lakse predju na taj program i da ga koriste, sire drugima.

Postoji dosta platformi gde se prevodi softver, jedna od njih je weblate, monero projekat se prevodi tamo na primer, evo linka. Da biste nasli gde da prevodite softver samo ukucajte <ime projekta> <localization>, na primer za Thunderbird, Gajim i bilo koji FOSS program/platformu koju koristite.

Ovo dosta doprinosi iako mozda deluje malo i nebitno.
## Pisanje dokumentacije

Mozete pisati dokumentaciju za softver koji korisite, preneti znanje dalje. Dosta dokumentacije zna da bude sturo i objasnjeno tako da zahteva vec neko prethodno znanje vi to mozete poboljasti. Ovo je povezano sa tackom lokalizacije softvera, dokumentacija na lokalnom jeziku dosta znaci ljudima.
Za ovo pogledajte wiki/dokumentaciju softver i pitajte preko mejla/kanala komunikacije koji koriste kako da doprinesete. Ako imaju samo na engleskom, tim i “bolje”, izdejstvujte da postoji i na drugim jezicima.

## Predlozi za poboljsanje

U svakom softveru koji koristimo vidimo mane. Te mane mozete istaci i otvoriti na koji softver koristi. Ovo dosta doprinosi jer dosta softvera ima problema sa dizajnom i bude tezak za upotrebu prosecnom korisniku. To se menja tako sto korisnici predlazu nove stvari za poboljsanje. Takodje ne dolaze iz programeskog sveta(progameri cesto znaju da ne uzimaju neke stvari u obzir i takodje da pretpostavljaju stvari o ljudima da su bolji sa tehnologijom nego sto jesu, ovo je sada sira tema ali pominjem i ovde jer je bitna),
Sirenje ideje, agitacija

Ovo je povezano sa prvom tezom. Sama upotreba FOSS programa dovodi do sirenja istih. Slobodan softver je jako bitan za sve nas. I korisnost slobodnog softvera dolazi kroz zajednicku upotrebu. Da bismo izbegli privatne i drzavne kompanije koje skupljaju informacije o nama, nasim navikama, zivotu i ko zna sta jos jedina odbrana su slobodan softver, slobodne mreze i zajednica koja to koristi i poboljsava.

Takodje, ako niste u stanju da odmah prebacite prijatelje na slobodne alternative, ubedite ih da barem instaliraju alternative i budu dostupni na njima.
To je jako bitan pocetak koji omogucava drugima da lakse predju, jer imaju vise ljudi koji je vec dostupan na tim alternativama.

Prebacite svoju mrezu prijatelja na slobodne “alternative” za mejl, fajlove, chat, video pozive itd. Time se siri ova ideja i poboljsava stanje digitalnog sveta. Samo na taj nacin se mozemo izboriti da tehnologija bude koriscena za nas, obicne ljude, a ne kompanije koje profitiraju na razne nemoralne nacine i produbljuju probleme u kojima se kao danasnje drustvo nalazimo.
Pokretanje lokalne zajednice

U vasoj lokalnoj zajednici mozete pokrenuti svoj “hackerspace”, grupu/mesto ljudi koja se bavi slobodnim softverom. Zajedno se bavite ovim tezama pre, ili ako imate administratora/programera pokrecete svoje servise(XMPP, Mastodon, Pixelfed, NextCloud npr.). Time omogucujete alternative kompanijama koje dolaze od lokalnih ljudi, lokalnih mreza. Sirenje ovakvih mreza je dosta znacajno. Za “hackerspace” nije potrebno puno, samo par ljudi od akcije, softver postoji vec fale samo ljudi.

# Hack The Planet!