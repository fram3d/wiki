---
title: Wiki.js
description: Proces instalacije i konfiguracije wiki.js platforme
published: false
date: 2023-02-05T17:19:49.809Z
tags: 
editor: markdown
dateCreated: 2023-02-05T17:13:24.056Z
---

# Wiki.js

Pretpostavljamo da vas (debian-based sa systemd-om) server poseduje:
	- node
	- npm
	- wget
 	- tar
  
Sve od potrebnog je moguce instalirati koriscenjem komande `sudo apt install node npm wget tar`

Takodje, Wiki.js podrzava nekoliko opcija za bazu podataka; na primer: `PostgreSQL`, `MySQL`, `SQLite`...
Mi koristimo `PostgreSQL`.

## Instalacija

Prvo preuzimamo trenutnu vereziju wiki.js softvera.

```
wget https://github.com/Requarks/wiki/releases/latest/download/wiki-js.tar.gz
```

Zatim pravimo novi folder, ekstraktujemo preuzetu arhivu u taj folder i ulazimo u taj folder.

```
mkdir wiki
tar xzf wiki-js.tar.gz -C ./wiki
cd ./wiki
```

Zatim kreiramo `config.yml` fajl i popunjavamo ga sa potrebnim opcijama

```yml
port: 3000 # port na kome ce wiki.js server da ceka/slusa request-ove
db: # podesavanje baze podataka
	type: postgres
  host: localhost # adresa na kojoj se nalazi postgresql server
  port: 5432 # port na kome postgresql server ocekuje request-ove
  user: wiki # username za bazu (koji treba napraviti)
  pass: password # password za dati nalog na postgresql serveru
  ssl: true
  sslOptions:
  	auto: false
    ca: /root/wiki/wiki.crt # sertifikat za ssl koji treba kreirati
    cert: /root/wiki/wiki.crt
  schema: public
ssl:
	enabled: false
  port: 3443
  
bindIP: 0.0.0.0
logLevel: info
logFormat: default
offline: false
ha: false
dataPath: ./data
bodyParserLimit: 5mb
```

Zatim kreiramo `wiki.service` fajl sa opcijama za systemd servis, unutar direktorijuma `/etc/systemd/system`.

```
[Unit]
Description=Decentrala's Wiki
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/node server
Restart=always
Environment=NODE_ENV=production
WorkingDirectory=/root/wiki

[Install]
WantedBy=multi-user.target
```

Ponovo pokrecemo systemd:
```bash
sudo systemctl daemon-reload
```

Pokrecemo wiki servis, i namestamo ga da se nadalje sam pokrene pri svakom ponovnom pokretanju masine (servera).
```bash
sudo systemctl start wiki
sudo systemctl enable wiki
```

Potrebno je namestiti zeljeni domen ka serveru na kojem se wiki.js pokrece, i ostatak podesavanja izvrsiti odlaskom na adresu na kojoj server ocekuje request-ove (npr. `https://wiki.dmz.rs/3000`)